<?php
/**
 * Display Orphans Plugin (Syntax Component)
 *
 * Description: The Display Orphans Plugin can display tables of orphaned, wanted, and linked pages.
 *
 * Syntax: <<displayorphans)>>
 *         <<displaywanted)>>
 *         <<displaylinked)>>
 *
 * @license    The MIT License (https://opensource.org/licenses/MIT)
 * @author     Jay Jeckel <jeckelmail@gmail.com>
 *
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
 */

if (!defined('DOKU_INC')) { die(); }

require_once(DOKU_INC . 'inc' . '/' . 'search.php');
require_once(dirname(__FILE__) . '/'. '_local.php');
use plugin\displayorphans\PageType;

class syntax_plugin_displayorphans extends DokuWiki_Syntax_Plugin
{
    public function __construct()
    {
        $this->helper = plugin_load('helper', 'displayorphans');
        $this->renderer = plugin_load('renderer', 'displayorphans');
    }

    public $helper;

    public $renderer;

    function getInfo() { return confToHash(dirname(__FILE__) . '/plugin.info.txt'); }

    function getType() { return 'substition'; }

    function getPType() { return 'block'; }

    function getSort() { return 5; }// Must come before {{media}} pattern is formatted at 320.

    function connectTo($mode)
    {
        $pattern = '<<display\s(?:' . PageType::ORPHAN . '|' . PageType::WANTED . '|' . PageType::LINKED . ')>>';
        $this->Lexer->addSpecialPattern($pattern, $mode, 'plugin_displayorphans');
    }

    function handle($match, $state, $pos, Doku_Handler $handler)
    {
        $match = substr($match, 9, -2);
        $type = trim($match);
        return array($type);
    }

    function render($format, Doku_Renderer $renderer, $data)
    {
        global $conf;

        $renderer->nocache();
        if ($format != 'xhtml') { return false; }

        list($type) = $data;
        $items = $this->helper->items($conf['datadir'], $type);
        $showHeader = $this->getConf('show_table_header');
        $showColumns = array(true, true, $type != PageType::WANTED, $type != PageType::ORPHAN);
        $this->renderer->table($renderer, $type, $items, $showHeader, $showColumns);

        return true;
    }
}

//Setup VIM: ex: et ts=4 enc=utf-8 :
?>