<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

namespace plugin\displayorphans;

final class PageType
{
    private function __construct() { }

    const ORPHAN = 'orphaned';

    const WANTED = 'wanted';

    const LINKED = 'linked';
}

final class Util
{
    private function __construct() { }

    public static function /* array */ asList(/* string */ $text, /* string */ $delimiter = ' ', /* int */ $limit = PHP_INT_MAX)
    { return empty(text) ? null : explode($delimiter, $text, $limit); }
}

final class Logic
{
    private function __construct() { }

    public static function /* bool */ isPageIgnored(/* string */ $id, array $ignoredPages = null, array $ignoreNamespaces = null)
    {
        if (!empty($ignoredPages) && in_array($id, $ignoredPages)) { return true; }
        if (!empty($ignoreNamespaces))
        {
            foreach ($ignoreNamespaces as $key => $namespace)
            {
                $namespace = rtrim($namespace, ':') . ':';
                $sub = substr($id, 0, strlen($namespace));
                if ($namespace == $sub) { return true; }
            }
        }
        return false;
    }

    public static function /* bool */ isOrphanedPage(/* string */ $id, array $item,
    array $ignoredPages = null, array $ignoreNamespaces = null)
    { return $item['exists'] && $item['count'] == 0 && !Logic::isPageIgnored($id, $ignoredPages, $ignoreNamespaces); }

    public static function /* bool */ isWantedPage(/* string */ $id, array $item,
    array $ignoredPages = null, array $ignoreNamespaces = null)
    { return !$item['exists'] && $item['count'] > 0 && !Logic::isPageIgnored($id, $ignoredPages, $ignoreNamespaces); }

    public static function /* bool */ isLinkedPage(/* string */ $id, array $item,
    array $ignoredPages = null, array $ignoreNamespaces = null)
    { return $item['exists'] && $item['count'] > 0 && !Logic::isPageIgnored($id, $ignoredPages, $ignoreNamespaces); }
}

?>