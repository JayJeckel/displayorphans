<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$lang['show_table_header'] = "Should generated tables include header elements? Default: Yes";
$lang['sort_table_ascending'] = "Should generated tables be sorted in ascending order? Default: Yes";

$lang['ignore_orphaned_pages'] = "Space-separated list of pages to ignore when generating tables of orphaned pages. Default: empty";
$lang['ignore_orphaned_namespaces'] = "Space-separated list of namespaces to ignore when generating tables of orphaned pages. Default: empty";
$lang['ignore_wanted_pages'] = "Space-separated list of pages to ignore when generating tables of wanted pages. Default: empty";
$lang['ignore_wanted_namespaces'] = "Space-separated list of namespaces to ignore when generating tables of wanted pages. Default: empty";
$lang['ignore_linked_pages'] = "Space-separated list of pages to ignore when generating tables of linked pages. Default: empty";
$lang['ignore_linked_namespaces'] = "Space-separated list of namespaces to ignore when generating tables of linked pages. Default: empty";

?>