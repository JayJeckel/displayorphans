<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$lang['text_show_backlinks'] = "Show Backlinks";
$lang['header_index'] = "#";
$lang['header_id'] = "ID";
$lang['header_title'] = "Title";
$lang['header_backlinks'] = "Backlinks";

?>