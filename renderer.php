<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/
 
if (!defined('DOKU_INC')) { die(); }

require_once(DOKU_INC . 'inc' . '/' . 'search.php');
require_once(dirname(__FILE__) . '/'. '_local.php');

class renderer_plugin_displayorphans extends DokuWiki_Plugin
{
    function /* void */ table(Doku_Renderer $renderer, /* string */ $type, array $items = null, /* bool */ $showHeader = null, array $showColumns = array(true, true, true, true))
    {
        $renderer->table_open(null, null, null, array('plugin__displayorphans_' . $type));

        if ($showHeader)
        {
            $this->_head($renderer, array(
                $showColumns[0] ? $this->getLang('header_index') : null,
                $showColumns[1] ? $this->getLang('header_id') : null,
                $showColumns[2] ? $this->getLang('header_title') : null,
                $showColumns[3] ? $this->getLang('header_backlinks') : null
                ));
        }

        $renderer->tabletbody_open();
        
        if (!empty($items))
        {
            foreach ($items as $id => $item)
            { $this->_row($renderer, $id, $item, ++$rownum, $type, $showColumns); }
        }

        $renderer->tabletbody_close();
        $renderer->table_close(null);
    }

    function /* void */ _head(Doku_Renderer $renderer, array $labels)
    {
        if (!empty($labels))
        {
            $renderer->tablethead_open();
            $renderer->tablerow_open();
            foreach ($labels as $label)
            {
                if ($label !== null)
                {
                    $renderer->tableheader_open();
                    $renderer->doc .= $label;
                    $renderer->tableheader_close();
                }
            }
            $renderer->tablerow_close();
            $renderer->tablethead_close();
        }
    }

    function /* void */ _row(Doku_Renderer $renderer, $id, $item, $rownum, $type, array $showColumns = array(true, true, true, true))
    {
        $renderer->tablerow_open();

        // Index of the row.
        if ($showColumns[0])
        {
            $renderer->tablecell_open();
            $renderer->doc .= $rownum;
            $renderer->tablecell_close();
        }

        // Link to page.
        if ($showColumns[1])
        {
            $renderer->tablecell_open();
            $renderer->internallink(':' . $id, $id);
            $renderer->tablecell_close();
        }

        // Title of the page.
        if ($showColumns[2])
        {
            $renderer->tablecell_open();
            if ($item['exists']) { $renderer->doc .= p_get_first_heading($id); }
            $renderer->tablecell_close();
        }
        
        // Backlinks link for the page.
        if ($showColumns[3])
        {
            $renderer->tablecell_open();
            if ($item['count'] > 0) { $this->_backlinksLink($renderer, $id, $item['count']); }
            $renderer->tablecell_close();
        }

        /*$renderer->tablecell_open();
        if ($type == PageType::ORPHAN) { $renderer->doc .= p_get_first_heading($id); }
        else if ($type == PageType::WANTED) { $this->_backlinksLink($renderer, $id, $item['count']); }
        else { }
        $renderer->tablecell_close();*/


        $renderer->tablerow_close();
    }

    function /* void */ _backlinksLink(Doku_Renderer &$renderer, $id, $count)
    {
        global $conf;
        
        $text = $this->getLang('text_show_backlinks');
        $link = array('target' => $conf['target']['wiki'], 'class' => 'wikilink1', 'url' => wl($id, 'do=backlink'), 'name' => $text, 'title' => $text . ' - ' . $id);
        if ($count !== null) { $renderer->doc .= $count . ' : '; }
        $renderer->doc .= $count > 0 ? $renderer->_formatLink($link) : $text;
    }
}
 
//Setup VIM: ex: et ts=4 enc=utf-8 :
?>