<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$conf['show_table_header'] = 1;
$conf['sort_table_ascending'] = 1;

$conf['ignore_orphaned_pages'] = '';
$conf['ignore_orphaned_namespaces'] = '';
$conf['ignore_wanted_pages'] = '';
$conf['ignore_wanted_namespaces'] = '';
$conf['ignore_linked_pages'] = '';
$conf['ignore_linked_namespaces'] = '';

?>